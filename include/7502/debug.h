/* vim: set ts=4 sw=4 tw=0 noet : */
#ifndef __DEBUG_H__
#define __DEBUG_H__

#if defined(__DEBUG__)
#define eprintf(str)	fprintf(stderr, "%s\n", str)
#else
#define eprintf(str)
#endif

#endif
