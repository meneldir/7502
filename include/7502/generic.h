/* vim: set ts=4 sw=4 tw=0 noet : */
#ifndef __GENERIC_H__
#define __GENERIC_H__

#define INIT_CHOP	1
#define CHOP_SIZE	7
#define FACT_PROP	3
#define FACT_ARIT	5

#define MAX_BUFFER	255
#define MAX_LONG_CADENA	(MAX_BUFFER - 1)

#endif
