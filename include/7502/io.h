/* vim: set ts=4 sw=4 tw=0 noet : */
#ifndef __IO_H__
#define __IO_H__
/* sistema */
#include <stdio.h>
/* comunes */
#include <7502/types.h>
#include <7502/generic.h>

/* lee un double del flujo fentrada */
status_t leer_double(FILE * fentrada, double * pdvar);

/* lee un double del flujo fentrada */
status_t leer_float(FILE * fentrada, float * pfvar);

/* lee una linea (hasta '\n' inclusive) de fentrada */
status_t leer_linea(FILE * fentrada, char ** linea);
#endif
