/* vim: set ts=4 sw=4 tw=0 noet : */
#ifndef __MEMDIN_H__
#define __MEMDIN_H__
#include <7502/types.h>
status_t leer_double(FILE * f, double * pd);
	/* lee un double de f y lo guarda en la posicion apuntada por pd. Si
	 * cualquiera es NULL, retorna ST_ERR_PUNTERO_NULO. Si f está cerrado,
	 * retorna ST_END_OF_FILE. Si no puede convertir lo leido a double, retorna
	 * ST_ERR_CONVERSION */
void free_vdoubles(double ** v, size_t * n);
void free_vpdoubles(double *** v, size_t * n);
	/* libera la memoria ocupada por v. Es silenciosa ante entradas nulas. Post
	 * condicion: carga NULL en v y 0 en n */
double * cargar_vdoubles1(FILE * f, size_t n);
double * cargar_vdoubles2(FILE * f, size_t * n);
	/* retorna un arreglo de n doubles leidos de f. Si n es puntero, lee de f
	 * hasta EOF y retorna la cantidad leida, en caso contrario espera leer n
	 * doubles, o retorna NULL. */
double ** cargar_vpdoubles1(FILE * f, size_t n);
double ** cargar_vpdoubles2(FILE * f, size_t * n);
	/* retorna un arreglo de n punteros a double leidos de f. Si n es puntero,
	 * lee de f hasta EOF y retorna la cantidad leida, en caso contrario espera
	 * leer n doubles, o retorna NULL. */
#endif
