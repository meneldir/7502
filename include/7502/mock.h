/* vim: set ts=4 sw=4 tw=0 noet : */
#ifndef __MOCK_H__
#define __MOCK_H__
/* sistema */
#include <stdio.h>

FILE * mock_textfile(const char * content);
FILE * mock_binfile(const void * content, size_t size, size_t nobj);
#endif
