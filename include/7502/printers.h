/* vim: set ts=4 sw=4 tw=0 noet : */
#ifndef __PRINTERS_H__
#define __PRINTERS_H__
#include <7502/types.h>
void print_vdoubles(FILE *, const double *, size_t);
void print_vpdoubles(FILE *, const double **, size_t);
void print_filadoubles(FILE *, const double *, size_t);
void print_matrizdoubles(FILE *, const double **, size_t, size_t);
#endif

