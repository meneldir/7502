/* vim: set ts=4 sw=4 tw=0 noet : */
#ifndef __TYPES_H__
#define __TYPES_H__

typedef enum status {
	ST_OK = 0,
	ST_END_OF_FILE,
	ST_LINEA_VACIA,
	ST_ERR_CONVERSION,
	ST_ERR_PUNTERO_NULO,
	ST_ERR_MEMORIA_AGOTADA,
	ST_MAX_ESTADO
} status_t;

typedef enum {
	FALSE  = 0,
	TRUE = !0
} bool_t;

#endif
