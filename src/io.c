/* vim: set ts=4 sw=4 tw=0 noet : */
/* sistema */
#include <stdio.h>
#include <stdlib.h>
/* comunes */
#include <7502/types.h>
#include <7502/generic.h>
/* especificos */
#include <7502/io.h>
#include <string.h>

/* lee un double del flujo fentrada */
status_t leer_double(FILE * fentrada, double * pdvar)
{
	char _s[MAX_LONG_CADENA];
	char * perr;
	if(!fentrada || !pdvar) return ST_ERR_PUNTERO_NULO;

	if(fgets(_s, MAX_LONG_CADENA, fentrada) == NULL) return ST_END_OF_FILE;
	if(*_s == '\n') return ST_LINEA_VACIA;
	*pdvar = strtod(_s, &perr);

	return (*perr == '\0' || *perr == '\n')? ST_OK : ST_ERR_CONVERSION;
}

/* lee un double del flujo fentrada */
status_t leer_float(FILE * fentrada, float * pfvar)
{
	double aux;
	status_t st;

	if(!pfvar) return ST_ERR_PUNTERO_NULO;

	if((st = leer_double(fentrada, &aux)) != ST_OK) return st;
	*pfvar = (float) aux; /* hace un downcast de double a float */

	return ST_OK;
}

/* lee una linea (hasta '\n' inclusive) de fentrada */
status_t leer_linea(FILE * fentrada, char ** linea)
{
	char * aux;
	size_t inicio;

	if((*linea = (char *)malloc((CHOP_SIZE + 1) * sizeof(char))) == NULL)
		return ST_ERR_MEMORIA_AGOTADA;

	inicio = 0;
	while(fgets(*linea + inicio, CHOP_SIZE + 1, fentrada) != NULL) {
		/* se termino de leer una linea ('\n') */ 
		/* ojo aca con archivos que contengan lineas vacias */
		if((*linea + inicio)[strlen(*linea + inicio) - 1] == '\n') break;

		inicio += CHOP_SIZE;
		if((aux = (char *)realloc(*linea, (inicio + 1 + CHOP_SIZE) * sizeof(char))) == NULL) {
			free(*linea);
			*linea = NULL;
			return ST_ERR_MEMORIA_AGOTADA;
		}
		*linea = aux;
	}

	if(ferror(fentrada)) {
		/* fgets() devolvió NULL porque fallo */
		free(*linea);
		*linea = NULL;
		return ST_ERR_MEMORIA_AGOTADA;
	}
	/* fgets() devolvio NULL porque alcanzo EOF */
	return ST_OK;
}
