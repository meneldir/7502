/* vim: set ts=4 sw=4 tw=0 noet : */
/* sistema */
#include <stdio.h>
#include <stdlib.h>
/* comunes */
#include <7502/types.h>
#include <7502/generic.h>
/* especificos */
#include <7502/io.h>
#include <7502/memdin.h>

/* libera la memoria pedida para un vector de doubles */
void free_vdoubles(double ** v, size_t * L)
{
	if(!v || !L) return;

	free(*v);
	*v = NULL;
	*L = 0;
}

/* libera la memoria pedida para un vector de PUNTEROS a double */
void free_vpdoubles(double *** v, size_t * L)
{
	size_t i;
	if(!v || !*v) return;

	for(i = 0; i < *L; i++)
		free((*v)[i]);
	free(*v);
	*v = NULL;
	if(L) *L = 0;
}

/* carga vector de doubles de longitud conocida */
double * cargar_vdoubles1(FILE * fentrada, size_t L)
{
	double * vp;
	size_t i;

	if(!fentrada) return NULL;

	vp = (double *) malloc(L * sizeof(double));
	if( vp == NULL ) return NULL;

	for(i = 0; i < L; i++){
		/* lectura del double */
		if(leer_double(fentrada, vp+i) != ST_OK){
			free_vdoubles(&vp, &i);
			return NULL;
		}
	}

	return vp;
}

/* carga vector de doubles mientras no se cierre el flujo (EOF) */
double * cargar_vdoubles2(FILE * fentrada, size_t * L)
{
	double * vd, * vdaux, dval;
	size_t rsz, asz;
	status_t st;

	if(!fentrada){ *L = 0; return NULL; }

	vd = (double *) malloc(INIT_CHOP * sizeof(double));
	if( vd == NULL ) return NULL;
	asz = INIT_CHOP;
	rsz = 0;

	for(rsz = 0; (st = leer_double(fentrada, &dval)) == ST_OK ; rsz++){
		/* chequear si es necesario pedir mas memoria */
		if( asz == rsz ){
			if((vdaux = (double *) realloc(vd, (FACT_PROP * asz) * sizeof(double))) == NULL){
				free_vdoubles(&vd, &rsz);
				*L = 0;
				return NULL;
			}
			vd = vdaux;
			asz *= FACT_PROP;
		}
		/* asignacion */
		vd[rsz] = dval;
	}

	if(st != ST_END_OF_FILE){
		free_vdoubles(&vd, &rsz);
		*L = 0;
		return NULL;
	}

	*L = rsz;
	return vd;
}

/* carga vector de PUNTEROS a double de longitud conocida */
double ** cargar_vpdoubles1(FILE * fentrada, size_t L)
{
	double ** vp;
	size_t rsz;
	double dval;
	status_t st;

	if(!fentrada) return NULL;

	vp = (double **) malloc(L * sizeof(double *));
	if( vp == NULL ) return NULL;

	for(rsz = 0; rsz < L; rsz++){
		/* lectura del double */
		if((st = leer_double(fentrada, &dval)) != ST_OK){
			free_vpdoubles(&vp, &rsz);
			return NULL;
		}

		/* pedido de la memoria necesaria */
		if((vp[rsz] = (double *) malloc(sizeof(double))) == NULL){
			free_vpdoubles(&vp, &rsz);
			return NULL;
		}

		/* asignacion */
		*(vp[rsz]) = dval;
	}
		
	return vp;
}

/* carga vector de PUNTEROS a doubles mientras no se cierre el flujo (EOF) */
double ** cargar_vpdoubles2(FILE * fentrada, size_t * L)
{
	double ** vp, ** vpaux;
	size_t rsz, asz;
	double dval;
	status_t st;

	if(!fentrada){ *L = 0; return NULL; }

	vp = (double **) malloc(INIT_CHOP * sizeof(double *));
	if( vp == NULL ) return NULL;
	asz = INIT_CHOP;

	for(rsz = 0; (st = leer_double(fentrada, &dval)) == ST_OK ; rsz++){
		/* chequear si es necesario pedir mas memoria */
		if( asz == rsz ){
			if((vpaux = (double **) realloc(vp, (FACT_PROP * asz) * sizeof(double *))) == NULL){
				free_vpdoubles(&vp, &rsz);
				*L = 0;
				return NULL;
			}
			vp = vpaux;
			asz *= FACT_PROP;
		}

		/* pedido de la memoria necesaria para un double */
		if((vp[rsz] = (double *) malloc(sizeof(double))) == NULL){
			free_vpdoubles(&vp, &rsz);
			*L = 0;
			return NULL;
		}
	
		/* asignacion */
		*(vp[rsz]) = dval;
	}

	if(st != ST_END_OF_FILE){
		free_vpdoubles(&vp, &rsz);
		*L = 0;
		return NULL;
	}

	*L = rsz;
	return vp;
}
