/* vim: set ts=4 sw=4 tw=0 noet : */
/* sistema */
#include <stdio.h>
/* debug: requiere __DEBUG__ */
#include <7502/debug.h>

FILE * mock_textfile(const char * content)
{
	FILE * real_file;

	if( (real_file = tmpfile()) == NULL ) return NULL;

	fprintf(real_file, "%s", content);

	if( fseek(real_file, 0L, SEEK_SET) ){
		eprintf("Error en fflush o en fseek");
		fclose(real_file);
		real_file = NULL;
	} else {
		clearerr(real_file);
	}

	return real_file;
}


FILE * mock_binfile(const void * content, size_t size, size_t nobj)
{
	FILE * real_file;

	if( (real_file = tmpfile()) == NULL ) return NULL;

	fwrite(content, size, nobj, real_file);

	if( fseek(real_file, 0L, SEEK_SET) ){
		fclose(real_file);
		real_file = NULL;
	} else {
		clearerr(real_file);
	}

	return real_file;
}
