/* vim: set ts=4 sw=4 tw=0 noet : */
/* sistema */
#include <stdio.h>
#include <stdlib.h>
/* comunes */
#include <7502/types.h>
#include <7502/generic.h>
/* especificos */
#include <7502/printers.h>

void print_vdoubles(FILE * fsalida, const double * vp, size_t len)
{
	size_t i;

	if(!vp) return;

	fprintf(fsalida, "%13p\n", (void *)vp);
	fprintf(fsalida, "        vp        -----> [%g]\n", vp[0]);
	for(i = 1; i < len; i++)
		fprintf(fsalida, "                         [%g]\n", vp[i]);
}

void print_vpdoubles(FILE * fsalida, const double ** vp, size_t len)
{
	size_t i;

	if(!vp) return;

	fprintf(fsalida, "%13p       %12p\n", (void *)vp, (void *)*vp);
	fprintf(fsalida, "        vp ------------> [%4lu] -----> (%g)\n", 0UL, *(vp[0]));
	for(i = 1; i < len; i++)
		fprintf(fsalida, "                         [%4lu] -----> (%g)\n", i, *(vp[i]));
}

void print_filadoubles(FILE * fsalida, const double * fila, size_t cols){
	size_t i;

	if(!fila) return;

	fprintf(fsalida, "(%p) [", (void *)fila);
	fprintf(fsalida, "%5g", fila[0]);
	for(i = 1; i < cols; i++)
		fprintf(fsalida, " | %5g", fila[i]);
	fputc(']', fsalida);
}

void print_matrizdoubles(FILE * fsalida, const double ** m, size_t fils, size_t cols){
	size_t i;

	if(!m) return;

	fprintf(fsalida, "%12p\n", (void *)m);
	for(i = 0; i < fils; i++){
		fprintf(fsalida, "      m --------> [%4lu] -->", i);
		print_filadoubles(fsalida, m[i], cols);
		fputc('\n', fsalida);
	}
}
