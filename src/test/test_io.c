/* vim: set ts=4 sw=4 tw=0 noet : */
/* sistema */
#include <stdio.h>
#include <stdlib.h>
/* comunes */
#include <7502/types.h>
#include <7502/generic.h>
#include <7502/mock.h>
/* test*/
#include <7502/test/minunit.h>
/* especificos */
#include <7502/io.h>
/* debug: requiere __DEBUG__ */
#include <7502/debug.h>

int tests_io = 0;
static char * test_leer_double(void)
{
	double dvar;
	status_t st;
	FILE * fentrada =
		mock_textfile("1\n3.93939393939393\n\n3e90\n3e-16\n2.0\n40\n44\n0.19\n1.\n.3\na\n0");

	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): 0) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_double(): 0) item 0", dvar == 1);
	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): 1) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_double(): 1) item 1", dvar == 3.93939393939393);
	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): retorno != ST_LINEA_VACIA", st == ST_LINEA_VACIA);
	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): 2) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_double(): 2) item 2", dvar == 3e90);
	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): 3) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_double(): 3) item 3", dvar == 3e-16);
	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): 4) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_double(): 4) item 4", dvar == 2.0);
	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): 5) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_double(): 5) item 5", dvar == 40);
	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): 6) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_double(): 6) item 6", dvar == 44);
	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): 7) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_double(): 7) item 7", dvar == 0.19);
	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): 8) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_double(): 8) item 8", dvar == 1.0);
	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): 9) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_double(): 9) item 9", dvar == .3);
	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): 10) retorno != ST_ERR_CONVERSION", st == ST_ERR_CONVERSION);
	st = leer_double(fentrada, &dvar);
	mu_assert("Fallo test_leer_double(): 11) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_double(): 11) item 11", dvar == 0);
	fclose(fentrada);
	return 0;
}

static char * test_leer_float(void)
{
	float fvar;
	status_t st;
	FILE * fentrada =
		mock_textfile("1\n3.93939393\n\n3e38\n3e-16\n2.0\n40\n44\n0.19\n1.\n.3\na\n0");

	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): 0) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_float(): 0) item 0", fvar == 1.f);
	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): 1) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_float(): 1) item 1", fvar == 3.93939393f);
	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): retorno != ST_LINEA_VACIA", st == ST_LINEA_VACIA);
	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): 2) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_float(): 2) item 2", fvar == 3e38f);
	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): 3) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_float(): 3) item 3", fvar == 3e-16f);
	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): 4) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_float(): 4) item 4", fvar == 2.0f);
	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): 5) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_float(): 5) item 5", fvar == 40.0f);
	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): 6) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_float(): 6) item 6", fvar == 44.f);
	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): 7) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_float(): 7) item 7", fvar == 0.19f);
	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): 8) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_float(): 8) item 8", fvar == 1.0f);
	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): 9) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_float(): 9) item 9", fvar == .3f);
	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): 10) retorno != ST_ERR_CONVERSION", st == ST_ERR_CONVERSION);
	st = leer_float(fentrada, &fvar);
	mu_assert("Fallo test_leer_float(): 11) retorno != ST_OK", st == ST_OK);
	mu_assert("Fallo test_leer_float(): 11) item 11", fvar == 0.f);
	fclose(fentrada);
	return 0;
}

static char * test_leer_linea(void)
{
	char * linea;
	status_t st;
	FILE * fentrada =
		mock_textfile("linea 1\nlinea 2 linea 2 linea 2 linea 2 linea 2 linea 2\n\nlinea 4\n");

	st = leer_linea(fentrada, &linea);
	mu_assert("Fallo test_leer_linea(): 0) retorno != ST_OK", st == ST_OK);
	free(linea);
	st = leer_linea(fentrada, &linea);
	mu_assert("Fallo test_leer_linea(): 1) retorno != ST_OK", st == ST_OK);
	free(linea);
	st = leer_linea(fentrada, &linea);
	mu_assert("Fallo test_leer_linea(): 2) retorno != ST_OK", st == ST_OK);
	free(linea);
	st = leer_linea(fentrada, &linea);
	mu_assert("Fallo test_leer_linea(): 3) retorno != ST_OK", st == ST_OK);
	free(linea);
	fclose(fentrada);
	return 0;
}

static char * test_io(void)
{
	mu_run_test(test_leer_double);
	mu_run_test(test_leer_float);
	mu_run_test(test_leer_linea);
	return 0;
}

#if defined(__TEST_UNIT_IO__)
int tests_run = 0;
int main(void)
{
	char * fail = test_io();

	if( fail != 0 ){
		fprintf(stderr, "%s\n", fail);
	} else {
		fprintf(stderr, "%s\n", "IO paso todas las pruebas");
	}
	tests_run += tests_io;
	fprintf(stderr, "Tests run: %i\n", tests_run);

	return fail != 0;
}
#endif
