/* vim: set ts=4 sw=4 tw=0 noet : */
/* sistema */
#include <stdio.h>
#include <stdlib.h>
/* comunes */
#include <7502/types.h>
#include <7502/generic.h>
#include <7502/mock.h>
/* test*/
#include <7502/test/minunit.h>
/* especificos */
#include <7502/memdin.h>
/* debug: requiere __DEBUG__ */
#include <7502/debug.h>

int tests_memdin = 0;
static char * test_cargar_vdoubles2_ok(void)
{
	double * vd;
	size_t L;
	FILE * fentrada =
		mock_textfile("1\n3.9393\n3e90\n3e-16\n2.0\n40\n44\n0.19\n1.\n.3");

	vd = cargar_vdoubles2(fentrada, &L);
	mu_assert("Fallo test_cargar_vpdoubles2(): retorno NULL", vd != NULL);
	mu_assert("Fallo test_cargar_vpdoubles2(): tamanio devuelto incorrecto", L == 10);

	mu_assert("Fallo test_cargar_vpdoubles2(): item 0", vd[0] == 1);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 1", vd[1] == 3.9393);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 2", vd[2] == 3e90);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 3", vd[3] == 3e-16);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 4", vd[4] == 2.0);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 5", vd[5] == 40);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 6", vd[6] == 44);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 7", vd[7] == 0.19);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 8", vd[8] == 1.0);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 9", vd[9] == .3);
	free_vdoubles(&vd, &L);
	fclose(fentrada);
	return 0;
}

static char * test_cargar_vdoubles2_data_incorrecta(void)
{
	double * vd;
	size_t L;
	FILE * fentrada =
		mock_textfile("1\na\n3e90\n3e-16\n2.0\n40\n44\n0.19\n1.\n.3");

	vd = cargar_vdoubles2(fentrada, &L);
	mu_assert("Fallo test_cargar_vpdoubles2(): no retorno NULL (debia \
hacerlo)", vd == NULL);

	fclose(fentrada);
	return 0;
}

static char * test_cargar_vpdoubles2_ok(void)
{
	double ** vpd;
	size_t L;
	FILE * fentrada =
		mock_textfile("1\n3.9393\n3e90\n3e-16\n2.0\n40\n44\n0.19\n1.\n.3");

	vpd = cargar_vpdoubles2(fentrada, &L);
	mu_assert("Fallo test_cargar_vpdoubles2(): retorno NULL", vpd != NULL);
	mu_assert("Fallo test_cargar_vpdoubles2(): tamanio devuelto incorrecto", L == 10);

	mu_assert("Fallo test_cargar_vpdoubles2(): item 0", *(vpd[0]) == 1);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 1", *(vpd[1]) == 3.9393);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 2", *(vpd[2]) == 3e90);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 3", *(vpd[3]) == 3e-16);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 4", *(vpd[4]) == 2.0);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 5", *(vpd[5]) == 40);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 6", *(vpd[6]) == 44);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 7", *(vpd[7]) == 0.19);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 8", *(vpd[8]) == 1.0);
	mu_assert("Fallo test_cargar_vpdoubles2(): item 9", *(vpd[9]) == .3);
	free_vpdoubles(&vpd, &L);
	fclose(fentrada);
	return 0;
}

static char * test_cargar_vpdoubles2_data_incorrecta(void)
{
	double ** vpd;
	size_t L;
	FILE * fentrada =
		mock_textfile("1\na\n3e90\n3e-16\n2.0\n40\n44\n0.19\n1.\n.3");

	vpd = cargar_vpdoubles2(fentrada, &L);
	mu_assert("Fallo test_cargar_vpdoubles2(): no retorno NULL (debia \
hacerlo)", vpd == NULL);

	fclose(fentrada);
	return 0;
}

static char * test_memdin(void)
{
	mu_run_test(test_cargar_vdoubles2_ok);
	mu_run_test(test_cargar_vdoubles2_data_incorrecta);
	mu_run_test(test_cargar_vpdoubles2_ok);
	mu_run_test(test_cargar_vpdoubles2_data_incorrecta);
	return 0;
}

#if defined(__TEST_UNIT_MEMDIN__)
int tests_run = 0;
int main(void)
{
	char * fail = test_memdin();

	if( fail != 0 ){
		fprintf(stderr, "%s\n", fail);
	} else {
		fprintf(stderr, "%s\n", "Memdin va");
	}
	tests_run += tests_memdin;
	fprintf(stderr, "Tests run: %i\n", tests_run);

	return fail != 0;
}
#endif
