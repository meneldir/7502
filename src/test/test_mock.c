/* vim: set ts=4 sw=4 tw=0 noet : */
/* sistema */
#include <stdio.h>
/* test*/
#include <7502/test/minunit.h>
/* particular */
#include <7502/mock.h>
#include <string.h>
/* debug: requiere __DEBUG__ */
#include <7502/debug.h>

int tests_mock = 0;

static char * test_textfile(void)
{
	char lectura[100] = {'\0'};
	const char * s = "Probando el mock del archivo de texto:\n1\n2\n3\n4\n5";

	FILE * mock_file;
	mock_file = mock_textfile(s);
	mu_assert("Fallo mock_textfile(): retorno NULL", mock_file != NULL);
	while(fgets(lectura + strlen(lectura), 100, mock_file));
	mu_assert("Fallo mock_textfile(): la lectura fue distinta",
		!strcmp(s, lectura));
	fclose(mock);
	return 0;
} 

static char * test_binfile(void)
{
	float lectura[5];
	const float ptr[4] = {1.4f,1.6f,1.9f,.10f};
	FILE * mock;

	mock_file = mock_binfile(ptr, sizeof(float), 4);
	mu_assert("Fallo mock_binfile(): retorno NULL", mock_file != NULL);
	fread(lectura, sizeof(float), 4, mock_file);
	mu_assert("Fallo mock_binfile(): la lectura fue distinta",
		!memcmp(ptr, lectura, 4 * sizeof(float)));
	return 0;
} 

static char * test_mock(void)
{
	mu_run_test(test_textfile);
	mu_run_test(test_binfile);
	return 0;
}

#if defined(__TEST_UNIT_MOCK__)
int tests_run = 0;
int main(void)
{
	char * fail = test_mock();

	if( fail != 0 ){
		fprintf(stderr, "%s\n", fail);
	} else {
		fprintf(stderr, "%s\n", "Mock va");
	}
	tests_run += tests_mock;
	fprintf(stderr, "Tests run: %i\n", tests_run);

	return fail != 0;
}
#endif
